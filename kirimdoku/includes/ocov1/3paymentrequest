<h2 id='payment-request'>Payment Request</h2>
<table><thead>
<tr>
<th>Endpoint</th>
<th>HTTP Method</th>
<th>Definition</th>
</tr>
</thead><tbody>
<tr>
<td>/Suite/Receive</td>
<td>POST</td>
<td>Merchant submits payment request using HTML Post from Customer&#39;s browser to DOKU Hosted Payment Page</td>
</tr>
</tbody></table>

<h3>Request Parameter for Payment Request</h3>

<table><thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Length</th>
<th>Description</th>
</tr>
</thead><tbody>
<tr>
<td>MALLID <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>10</td>
<td>Merchant ID given by Doku</td>
</tr>
<tr>
<td>CHAINMERCHANT <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>8</td>
<td>Given by Doku. If not using Chain, use value: <code>NA</code></td>
</tr>
<tr>
<td>AMOUNT <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>12.2</td>
<td>Total amount. Eg: 10000.00</td>
</tr>
<tr>
<td>PURCHASEAMOUNT <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>12.2</td>
<td>Total amount. Eg: 10000.00</td>
</tr>
<tr>
<td>TRANSIDMERCHANT <h6 style="color:red;"><code>Required</code></h6></td>
<td>AN</td>
<td>30</td>
<td>Transaction ID from Merchant</td>
</tr>
<tr>
<td>PAYMENTTYPE</td>
<td>AN</td>
<td>10</td>
<td>Default is SALE</td>
</tr>
<tr>
<td>WORDS  <h6 style="color:red;"><code>Required</code></h6></td>
<td>AN</td>
<td>200</td>
<td>Hashed key combination encryption. The hashed key generated from combining these parameters value in this order:<br/> <code>AMOUNT+MALLID+SHAREDKEY+TRANSIDMERCHANT</code> For transaction with currency other than 360 (IDR), use: <br/> <code>AMOUNT+MALLID+SHAREDKEY+TRANSIDMERCHANT+CURRENCY</code> <br/> <em>For Shared Key, refer to Shared Key Hashed Value section.</em></td>
</tr>
<tr>
<td>REQUESTDATETIME <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>14</td>
<td><code>YYYYMMDDHHMMSS</code></td>
</tr>
<tr>
<td>CURRENCY <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>3</td>
<td>ISO3166, numeric code</td>
</tr>
<tr>
<td>PURCHASECURRENCY <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>3</td>
<td>ISO3166, numeric code</td>
</tr>
<tr>
<td>SESSIONID <h6 style="color:red;"><code>Required</code></h6></td>
<td>AN</td>
<td>48</td>
<td>Merchant can use this parameter for additional validation, DOKU will return the value in other response process.</td>
</tr>
<tr>
<td>NAME <h6 style="color:red;"><code>Required</code></h6></td>
<td>AN</td>
<td>50</td>
<td>Customer / Buyer name</td>
</tr>
<tr>
<td>EMAIL <h6 style="color:red;"><code>Required</code></h6></td>
<td>ANS</td>
<td>100</td>
<td>Customer / Buyer email</td>
</tr>
<tr>
<td>ADDITIONALDATA</td>
<td>ANS</td>
<td>1024</td>
<td>Custom additional data for specific merchant use</td>
</tr>
<tr>
<td>BASKET <h6 style="color:red;"><code>Required</code></h6></td>
<td>ANS</td>
<td>1024</td>
<td>Show transaction description. Use comma to separate each field and semicolon for each item. E.g.  <code>Item1,1000.00,2,20000.00;item2,15000.00,2,30000.00</code></td>
</tr>
<tr>
<td>SHIPPING_ADDRESS</td>
<td>ANS</td>
<td>100</td>
<td>Shipping address contains street and number</td>
</tr>
<tr>
<td>SHIPPING_CITY</td>
<td>ANS</td>
<td>100</td>
<td>City name</td>
</tr>
<tr>
<td>SHIPPING_STATE</td>
<td>AN</td>
<td>100</td>
<td>State or Province name</td>
</tr>
<tr>
<td>SHIPPING_COUNTRY</td>
<td>A</td>
<td>2</td>
<td>ISO3166, alpha</td>
</tr>
<tr>
<td>SHIPPING_ZIPCODE</td>
<td>N</td>
<td>10</td>
<td>ZIP Code</td>
</tr>
<tr>
<td>PAYMENTCHANNEL</td>
<td>N</td>
<td>2</td>
<td>See payment channel code list</td>
</tr>
<tr>
<td>ADDRESS</td>
<td>ANS</td>
<td>100</td>
<td>Home address contains street and number</td>
</tr>
<tr>
<td>CITY</td>
<td>ANS</td>
<td>100</td>
<td>City name</td>
</tr>
<tr>
<td>STATE</td>
<td>AN</td>
<td>100</td>
<td>State or Province name</td>
</tr>
<tr>
<td>COUNTRY</td>
<td>A</td>
<td>2</td>
<td>ISO3166, alpha</td>
</tr>
<tr>
<td>ZIPCODE</td>
<td>N</td>
<td>10</td>
<td>Zip Code</td>
</tr>
<tr>
<td>HOMEPHONE</td>
<td>ANS</td>
<td>11</td>
<td>Home Phone</td>
</tr>
<tr>
<td>MOBILEPHONE</td>
<td>ANS</td>
<td>12</td>
<td>Mobile Phone</td>
</tr>
<tr>
<td>WORKPHONE</td>
<td>ANS</td>
<td>13</td>
<td>Work Phone / Office Phone</td>
</tr>
<tr>
<td>BIRTHDATE</td>
<td>N</td>
<td>8</td>
<td>YYYYMMDD</td>
</tr>
</tbody></table>

<h3>Additional Parameters for Airlines</h3>

<p>Airlines merchant is required to send some additional specific parameters on payment request. Below are the parameters: </p>

<table><thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Length</th>
<th>Description</th>
</tr>
</thead><tbody>
<tr>
<td>FLIGHT <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>2</td>
<td><code>01</code> for Domestic, <code>02</code> for International</td>
</tr>
<tr>
<td>FLIGHTTYPE <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>1</td>
<td><code>0</code> : One Way, <code>1</code> : Return, <code>2</code> : Transit, <code>3</code> : Transit &amp; Return, <code>4</code> : Multiple Cities</td>
</tr>
<tr>
<td>BOOKINGCODE <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>20</td>
<td>Booking code generated by Airline</td>
</tr>
<tr>
<td>ROUTE <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>50</td>
<td>List of route using format <code>XXX-YYY</code> (from XXX to YYY). E.g. : <code>CGK-DPS</code></td>
</tr>
<tr>
<td>FLIGHTDATE <h6 style="color:red;"><code>Required</code></h6></td>
<td>AN</td>
<td>8</td>
<td>List of flight date using format <code>YYYYMMDD</code></td>
</tr>
<tr>
<td>FLIGHTTIME <h6 style="color:red;"><code>Required</code></h6></td>
<td>AN</td>
<td>6</td>
<td>List of flight time using format <code>HHMMSS</code></td>
</tr>
<tr>
<td>FLIGHTNUMBER <h6 style="color:red;"><code>Required</code></h6></td>
<td>AN</td>
<td>30</td>
<td>List of flight number using IATA Standard. <code>XXYYYY</code> (XX = Airline Name, YYYY = Flight Number)</td>
</tr>
<tr>
<td>PASSENGER_NAME <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>200</td>
<td>List of passenger name in one booking code</td>
</tr>
<tr>
<td>PASSENGER_TYPE <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>1</td>
<td>List of passenger type in one booking. <code>A</code> : Adult, <code>C</code> : Child</td>
</tr>
<tr>
<td>VAT <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>12.2</td>
<td>Total VAT. Eg: <code>10000.00</code></td>
</tr>
<tr>
<td>INSURANCE <h6 style="color:red;"><code>Required</code></h6></td>
<td>AN</td>
<td>12.2</td>
<td>Total Insurance. Eg: <code>10000.00</code></td>
</tr>
<tr>
<td>FUELSURCHARGE <h6 style="color:red;"><code>Required</code></h6></td>
<td>AN</td>
<td>12.2</td>
<td>Total fuel surcharge. Eg: <code>10000.00</code></td>
</tr>
<tr>
<td>THIRDPARTY_STATUS <h6 style="color:red;"><code>Required</code></h6></td>
<td>ANS</td>
<td>1</td>
<td><code>0</code> : Travel Arranger joins the flight, <code>1</code> : Travel Arranger does not join the flight</td>
</tr>
<tr>
<td>FFNUMBER <h6 style="color:red;"><code>Required</code></h6></td>
<td>ANS</td>
<td>16</td>
<td>Frequent Flyer Number</td>
</tr>
</tbody></table>

<h3>Additional Parameters for Credit Card Installment</h3>

<p>Credit Card Installment payment require some additional specific parameters on payment request. Below are the parameters:</p>

<table><thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Length</th>
<th>Description</th>
</tr>
</thead><tbody>
<tr>
<td>INSTALLMENT_ACQUIRER <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>3</td>
<td>Acquirer code for installment</td>
</tr>
<tr>
<td>TENOR <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>2</td>
<td>Number of month to pay the installment</td>
</tr>
<tr>
<td>PROMOID <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>3</td>
<td>Promotion ID from the bank for the current merchant</td>
</tr>
</tbody></table>

<h3>Additional Parameters for Tokenization (First Payment)</h3>

<p>In the DOKU Hosted API, Credit Card Tokenization is treated as a separate payment method from the un-tokenized Credit Card. Please send PAYMENTCHANNEL parameter in Payment Request with value <code>16</code>.</p>

<table><thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Length</th>
<th>Description</th>
</tr>
</thead><tbody>
<tr>
<td>CUSTOMERID <h6 style="color:red;"><code>Required</code></h6></td>
<td>AN</td>
<td>16</td>
<td>Merchant’s customer identifier</td>
</tr>
<tr>
<td>PAYMENTCHANNEL <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>2</td>
<td><code>16</code></td>
</tr>
</tbody></table>

<h3>Additional Parameters for Second Payment Request</h3>

<table><thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Length</th>
<th>Description</th>
</tr>
</thead><tbody>
<tr>
<td>CUSTOMERID <h6 style="color:red;"><code>Required</code></h6></td>
<td>AN</td>
<td>16</td>
<td>Merchant’s customer identifier</td>
</tr>
<tr>
<td>TOKENID <h6 style="color:red;"><code>Required</code></h6></td>
<td>AN</td>
<td>16</td>
<td>Tokenized Card Identifier</td>
</tr>
<tr>
<td>PAYMENTCHANNEL <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>2</td>
<td><code>16</code></td>
</tr>
</tbody></table>

<aside class="notice">If merchant sent Payment Channel `16` but did not send CUSTOMERID, DOKU will use EMAIL value as CUSTOMERID value.</aside>

<h3>Additional Parameters for Recurring (First Payment)</h3>

<p>In the DOKU-Hosted API, Credit Card Recurring is treated as a separate payment method. Please send PAYMENTCHANNEL parameter in Payment Request with value <code>17</code>.</p>

<table><thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Length</th>
<th>Description</th>
</tr>
</thead><tbody>
<tr>
<td>CUSTOMERID <h6 style="color:red;"><code>Required</code></h6></td>
<td>AN</td>
<td>16</td>
<td>Merchant’s customer identifier</td>
</tr>
<tr>
<td>BILLNUMBER <h6 style="color:red;"><code>Required</code></h6></td>
<td>AN</td>
<td>16</td>
<td>Merchant’s bill identifier</td>
</tr>
<tr>
<td>BILLDETAIL <h6 style="color:red;"><code>Required</code></h6></td>
<td>ANS</td>
<td>256</td>
<td>Product information</td>
</tr>
<tr>
<td>BILLTYPE <h6 style="color:red;"><code>Required</code></h6></td>
<td>A</td>
<td>1</td>
<td><code>S</code>= Shopping, <code>I</code> = Installment, <code>D</code> = Donation, <code>P</code> = Payment</td>
</tr>
<tr>
<td>STARTDATE <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>8</td>
<td>Recurring start date <code>yyyyMMdd</code></td>
</tr>
<tr>
<td>ENDDATE <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>8</td>
<td>Recurring end date <code>yyyyMMdd</code> , <code>NA</code> = end date not specified</td>
</tr>
<tr>
<td>EXECUTETYPE <h6 style="color:red;"><code>Required</code></h6></td>
<td>A</td>
<td>4</td>
<td><code>DAY</code> or <code>DATE</code> or <code>FULLDATE</code></td>
</tr>
<tr>
<td>EXECUTEDATE <h6 style="color:red;"><code>Required</code></h6></td>
<td>AN</td>
<td>3</td>
<td>If EXECUTETYPE = <code>DAY</code> then <code>SUN</code> / <code>MON</code> / <code>TUE</code> / <code>WED</code> / <code>THU</code> / <code>FRI</code> / <code>SAT</code> . If EXECUTETYPE = <code>DATE</code> then <code>1</code>/<code>2</code>/<code>3</code>/.../<code>28</code> . If EXECUTETYPE = <code>FULLDATE</code> then list of execute dates in <code>yyyyMMdd</code></td>
</tr>
<tr>
<td>EXECUTEMONTH <h6 style="color:red;"><code>Required</code></h6></td>
<td>A</td>
<td>3</td>
<td><code>JAN</code> / <code>FEB</code> / <code>MAR</code> / <code>APR</code> / <code>MAY</code> / <code>JUN</code> / <code>JUL</code> / <code>AUG</code> / <code>SEP</code> / <code>OCT</code> / <code>NOV</code> / <code>DEC</code></td>
</tr>
<tr>
<td>FLATSTATUS <h6 style="color:red;"><code>Required</code></h6></td>
<td>A</td>
<td>5</td>
<td>If the amount is dynamic, use value: <code>FALSE</code>. Use <code>TRUE</code> if the amount is fixed.</td>
</tr>
<tr>
<td>REGISTERAMOUNT</td>
<td>N</td>
<td>12.2</td>
<td>Registration amount Eg: <code>10000.00</code></td>
</tr>
</tbody></table>

<h3>Additional Parameters for Customer Input at Merchant</h3>

<p>Please send PAYMENTCHANNEL parameter in Payment Request with value <code>15</code> for Regular Payment, or <code>16</code> if merchant use Tokenization.</p>

<table><thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Length</th>
<th>Description</th>
</tr>
</thead><tbody>
<tr>
<td>CARDNUMBER <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>16</td>
<td>Credit Card Number</td>
</tr>
<tr>
<td>EXPIRYDATE <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>4</td>
<td>Credit Card Expiry Date <code>YYMM</code></td>
</tr>
<tr>
<td>CVV2 <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>4</td>
<td>Credit Card Security Number for not present transactions</td>
</tr>
<tr>
<td>CC_NAME</td>
<td>AN</td>
<td>50</td>
<td>Cardholder Name</td>
</tr>
<tr>
<td>PAYMENTCHANNEL <h6 style="color:red;"><code>Required</code></h6></td>
<td>N</td>
<td>2</td>
<td><code>15</code> for regular payment or <code>16</code> for tokenization</td>
</tr>
</tbody></table>
